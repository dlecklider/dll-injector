#ifndef DLLINJECTOR_HPP
#define DLLINJECTOR_HPP

#define WIN32_LEAN_AND_MEAN 

#include <iostream>
#include "Windows.h"

enum class LoadDLLStatus
{
	cSUCCESS,
	cFAIL_VIRTUALALLOC,
	cFAIL_WRITEMEMORY,
	cFAIL_GETPROCADDRESS,
	cFAIL_CREATEREMOTETHREAD
};

//! Look for the process name in the list of processes
//! Returns: Null if a failure occured. Non-Null if a valid process handle.
//! Modified from https://msdn.microsoft.com/en-us/library/windows/desktop/ms686701(v=vs.85).aspx
HANDLE FindProcess(const wchar_t* processName, std::wostream* aOutputPath = nullptr);

//! Load DLL into remote process
//! Gets LoadLibraryA address from current process, which is guaranteed to be same for single boot session across processes
//! Allocated memory in remote process for DLL path name
//! CreateRemoteThread to run LoadLibraryA in remote process. Address of DLL path in remote memory as argument
//!
LoadDLLStatus LoadRemoteDLL(HANDLE hProcess, const wchar_t* dllPath);

#endif