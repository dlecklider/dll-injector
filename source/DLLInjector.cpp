#include "DLLInjector.hpp"

#include "tlhelp32.h"

#define LOADLIBRARYW  "LoadLibraryW"
#define KERNEL32      "Kernel32.dll"

template <typename Arg, typename... Args>
void printErrorMessage(std::wostream* out, Arg&& arg, Args&& ... args)
{
	if (out)
	{
		*out << std::forward<Arg>(arg);
		using expander = int[];
		(void)expander {
			0, (void(*out << std::forward<Args>(args)), 0)...
		};
	}
}

HANDLE FindProcess(const wchar_t* processName, std::wostream* aOutputStream)
{
	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;
	
	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	
	if (hProcessSnap == INVALID_HANDLE_VALUE) 
	{
		printErrorMessage(aOutputStream, TEXT("Process snapshot could not be created.\n"));
	}

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process; exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32)) 
	{
		CloseHandle(hProcessSnap);
		return nullptr;
	}

	// Now walk the snapshot of processes
	do 
	{
		if (!wcscmp(pe32.szExeFile, processName)) 
		{
			hProcess = OpenProcess(PROCESS_VM_WRITE | PROCESS_VM_OPERATION | PROCESS_CREATE_THREAD, FALSE, pe32.th32ProcessID);
			if (hProcess != nullptr) 
			{
				return hProcess;
			}
			else 
			{
				printErrorMessage(aOutputStream, TEXT("Failed to open process "), pe32.szExeFile, TEXT("\n"));
				return nullptr;
			}
		}
	} while (Process32Next(hProcessSnap, &pe32));

	printErrorMessage(aOutputStream, processName, TEXT(" has not been loaded into memory, aborting.\n"));
	return nullptr;
}

LoadDLLStatus LoadRemoteDLL(HANDLE hProcess, const wchar_t* dllPath)
{
	// Eventually we will want to pass in an optional handle to throw error messages into. But for now just create a turned off conditional statement.
	bool printErrorMsg = false;

	wchar_t pathBuffer[MAX_PATH];
	size_t stringSize = sizeof(pathBuffer);

	ZeroMemory(pathBuffer, stringSize);
	GetFullPathNameW(dllPath, 260, pathBuffer, nullptr);

	// Allocate memory for DLL's path name to remote process
	LPVOID dllPathAddressInRemoteMemory = VirtualAllocEx(hProcess, nullptr, stringSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

	if (dllPathAddressInRemoteMemory == nullptr)
	{
		return LoadDLLStatus::cFAIL_VIRTUALALLOC;
	}

	// Write DLL's path name to remote process
	BOOL succeededWriting = WriteProcessMemory(hProcess, dllPathAddressInRemoteMemory, pathBuffer, stringSize, nullptr);

	if (!succeededWriting)
	{
		return LoadDLLStatus::cFAIL_WRITEMEMORY;
	}
	else
	{
		// Returns a pointer to the LoadLibrary address. This will be the same on the remote process as in our current process.
		LPVOID loadLibraryAddress = (LPVOID)GetProcAddress(GetModuleHandle(TEXT(KERNEL32)), LOADLIBRARYW);

		if (loadLibraryAddress == nullptr)
		{
			return LoadDLLStatus::cFAIL_GETPROCADDRESS;
		}
		else
		{
			HANDLE remoteThread = CreateRemoteThread(hProcess, nullptr, 0, static_cast<LPTHREAD_START_ROUTINE>(loadLibraryAddress), dllPathAddressInRemoteMemory, 0, nullptr);

			if (remoteThread == nullptr)
			{
				return LoadDLLStatus::cFAIL_CREATEREMOTETHREAD;
			}

			WaitForSingleObject(remoteThread, INFINITE);
			VirtualFreeEx(hProcess, loadLibraryAddress, 0, MEM_RELEASE);
		}
	}

	CloseHandle(hProcess);
	return LoadDLLStatus::cSUCCESS;
}